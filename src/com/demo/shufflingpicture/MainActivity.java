package com.demo.shufflingpicture;

import java.util.LinkedList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity {
	private ShufflingPicture shufflingPicture;
	private LinkedList<View> views;
	private LinkedList<Intent> viewIntents;
	private LayoutInflater mInflater; // 用于解XML
	private int[] strings = new int[] { R.drawable.image_1, R.drawable.image_2,
			R.drawable.image_3, R.drawable.image_4 , R.drawable.image_5};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		initView();
		
	}
	private void initView() {
		mInflater = LayoutInflater.from(this);
		views = new LinkedList<View>();
		viewIntents = new LinkedList<Intent>();
		for(int i=0;i<strings.length;i++){
			ImageView view = (ImageView) mInflater.inflate(
					R.layout.imageview, null);
			view.setImageResource(strings[i]);
			views.add(view);
			// 跳转到课程详情传的bundle数据，课程id
			Bundle bundle = new Bundle();
			bundle.putInt("number", i);
			Intent intent = new Intent(this, OneActivity.class);
			intent.putExtras(bundle);
			viewIntents.add(intent);
		}
		
		shufflingPicture = (ShufflingPicture) findViewById(R.id.shuffling_picture);
		shufflingPicture.setViews(views,viewIntents);
	}

}
