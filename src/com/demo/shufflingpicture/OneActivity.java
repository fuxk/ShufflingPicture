package com.demo.shufflingpicture;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class OneActivity extends Activity {
	private TextView number;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_one);
		number = (TextView) findViewById(R.id.number);
		number.setText(Integer.toString(getIntent().getExtras().getInt("number")));
		
	}

}
